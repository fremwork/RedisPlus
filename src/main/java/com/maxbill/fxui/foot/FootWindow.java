package com.maxbill.fxui.foot;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Paint;

import static com.maxbill.fxui.util.CommonConstant.*;

/**
 * 底部窗口
 *
 * @author MaxBill
 * @date 2019/07/06
 */
public class FootWindow {

    private static Label footImage;
    private static Label footTitle;
    private static Label footOrder;

    public static GridPane getFootWindow() {
        GridPane footWindow = new GridPane();
        footWindow.setId("foot-pane");
        footWindow.getStylesheets().add(STYLE_FOOT);

        footImage = new Label();
        footTitle = new Label();
        footOrder = new Label();

        footImage.setId("foot-view-image");
        footTitle.setId("foot-view-title");
        footOrder.setId("foot-view-order");

        footImage.setPrefSize(27, 23);
        footTitle.setPrefSize(27, 23);
        footOrder.setPrefSize(27, 23);

        footTitle.setMinWidth(200.00);
        footOrder.setMinWidth(150.00);
        footTitle.setText(MESSAGE_STATUE_NO);
        footOrder.setText(APP_VERSION);

        footWindow.add(footImage, 0, 0);
        footWindow.add(footTitle, 1, 0);
        footWindow.add(footOrder, 2, 0);

        footWindow.setPadding(new Insets(5));
        footTitle.setTextFill(Paint.valueOf("red"));
        footOrder.setTextFill(Paint.valueOf("#1766A2"));
        footImage.setAlignment(Pos.CENTER);
        footTitle.setAlignment(Pos.CENTER_LEFT);
        footOrder.setAlignment(Pos.CENTER_RIGHT);
        footImage.setGraphic(new ImageView(new Image(FOOT_ICON_01)));
        GridPane.setHgrow(footTitle, Priority.ALWAYS);

        return footWindow;
    }

    /**
     * 修改版本信息
     *
     * @param version 版本号
     */
    public static void changeFootState(String version) {
        footOrder.setText(version);
    }

    /**
     * 需改连接状态信息
     *
     * @param isOpen 是否连接成功
     * @param msg    错误信息
     */
    public static void changeFootState(boolean isOpen, String msg) {
        if (!isOpen) {
            footImage.setGraphic(new ImageView(new Image(FOOT_ICON_01)));
            footTitle.setText(msg);
            footTitle.setTextFill(Paint.valueOf("red"));
        } else {
            footImage.setGraphic(new ImageView(new Image(FOOT_ICON_02)));
            footTitle.setText(msg);
            footTitle.setTextFill(Paint.valueOf("green"));
        }
    }

}

package com.maxbill.base.bean;


import lombok.Data;

@Data
public class Message {

    private String time;
    private String type;
    private String body;
    private String className;
    private String exception;

}
